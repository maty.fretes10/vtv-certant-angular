import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { InspectorModel } from 'src/app/models/inspector.model';
import { InspectorServiceService } from 'src/app/services/inspector-service.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-inspector',
  templateUrl: './inspector.component.html',
  styleUrls: ['./inspector.component.css']
})
export class InspectorComponent implements OnInit {

  inspector = new InspectorModel();
  error: boolean;

  constructor(private inspectorServiceService : InspectorServiceService,
    private route: ActivatedRoute) { 
      this.error = false;
    }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id') || '';

      if( id !== 'nuevo' ) {
          this.inspectorServiceService.getInspector(Number(id))
          .subscribe( (resp: any) => {
            this.inspector = resp;
            this.inspector.id = Number(id);
          });
      }
  }

  guardar( form: NgForm ){
    
    if(form.invalid){
      Object.values(form.controls).forEach(control => {
        control.markAllAsTouched();
      })
      return;
    }

    Swal.fire({
      title: 'Espere',
      text:'Guardando información',
      icon: 'info',
      allowOutsideClick:false
    });
    Swal.showLoading();

    let peticion: Observable<any>;

    if ( this.inspector.id ) {
      peticion = this.inspectorServiceService.editarInspector( this.inspector );
    } else {
      peticion = this.inspectorServiceService.crearInspector( this.inspector );
    }

    peticion.subscribe(resp => {
      Swal.fire({
        title: this.inspector.nombre + ' ' + this.inspector.apellido,
        text: 'Se actualizó correctamente',
        icon: 'success'
      });
    }, (errorServicio) => {
      this.error = true;
      Swal.fire({
        title: 'No se pudo actualizar',
        text: 'El legajo ya existe',
        icon: 'warning'
      });
    });
  }
}
