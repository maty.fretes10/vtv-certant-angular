import { Component, OnInit } from '@angular/core';
import { InspectorModel } from 'src/app/models/inspector.model';
import { InspectorServiceService } from 'src/app/services/inspector-service.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-inspectores',
  templateUrl: './inspectores.component.html',
  styleUrls: ['./inspectores.component.css']
})
export class InspectoresComponent implements OnInit {

  inspectores: InspectorModel[] = [];
  cargando = false;

  constructor( private inspectorService : InspectorServiceService) { }

  ngOnInit(){

    this.cargando = true;

    this.inspectorService.getInspectores().subscribe((resp: any) => {
      this.inspectores = resp;
      this.cargando = false;
    });
  }

  inactivarInspector(inspector: InspectorModel, i:number) {

    Swal.fire({
      title: '¿Está seguro?',
      text: `Está seguro que desea borrar a ${inspector.nombre} ${inspector.apellido}`,
      icon:'question',
      showConfirmButton: true,
      showCancelButton:true 
    }).then(resp => {
      if(resp.value){
        this.inspectores.splice(i, 1);
        this.inspectorService.inactivarInspector(Number(inspector.id)).subscribe();
      }
    });

  }
}
