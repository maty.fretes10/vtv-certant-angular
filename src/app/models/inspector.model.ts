
export class InspectorModel{
    id: number | undefined;
    nombre: string | undefined;
    apellido: string | undefined;
    dni: string | undefined;
    legajo: string | undefined;
}