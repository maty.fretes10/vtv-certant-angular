import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {InspectoresComponent} from './pages/inspectores/inspectores.component';
import {InspectorComponent} from './pages/inspector/inspector.component';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms';
import { HttpClientModule} from '@angular/common/http';


@NgModule({
  declarations: [
    AppComponent,
    InspectorComponent,
    InspectoresComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
