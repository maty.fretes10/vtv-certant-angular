import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule, Routes } from "@angular/router";
import {InspectoresComponent} from './pages/inspectores/inspectores.component';
import {InspectorComponent} from './pages/inspector/inspector.component';


const routes : Routes = [
    {path: 'inspectores', component: InspectoresComponent},
    {path: 'inspector/:id', component: InspectorComponent},
    {path: '**', pathMatch:'full', redirectTo:'inspectores'}
]

@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ],
    exports: [
        RouterModule
    ]
})

export class AppRoutingModule{}