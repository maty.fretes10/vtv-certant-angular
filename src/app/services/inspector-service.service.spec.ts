import { TestBed } from '@angular/core/testing';

import { InspectorServiceService } from './inspector-service.service';

describe('InspectorServiceService', () => {
  let service: InspectorServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(InspectorServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
