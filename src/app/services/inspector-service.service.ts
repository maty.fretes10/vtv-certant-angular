import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { InspectorModel } from '../models/inspector.model';
import { delay } from 'rxjs/operators'; 

@Injectable({
  providedIn: 'root'
})
export class InspectorServiceService {

  private url = 'http://localhost:8080/api/v1'

  constructor( private http: HttpClient) { }

  crearInspector( inspector: InspectorModel ) {
    return this.http.post(`${ this.url}/inspector`, inspector);
  }

  inactivarInspector(id: number) {
    return this.http.delete(`${ this.url}/inspector/delete?id=${id}`);
  }

  editarInspector(inspector: InspectorModel){
    return this.http.post(`${ this.url}/inspector/edit?id=${inspector.id}`, inspector);
  }

  getInspectores(){
      return this.http.get(`${this.url}/inspectores`).pipe(delay(1000));
  }

  getInspector(id: number){
    return this.http.get(`${ this.url}/inspector?id=${id}`)
  }
}
